<center><h1>Cognitus Project</h1></center>

[
    <center>
    <img src="./.public/cognitus.png" alt="Cognitus" style="width: 10em; border-radius: 1em;"/>
    </center>
](https://www.cognitus.org/ "Cognitus Fanpage.")

__________________________

## Starting

* _Go to 'cognitus-project' directory_
* _Open one terminal and execute:_

```
npm install --prefix api/ && node api/server.js
```
* _Then go to the 'view' directory and open the index.html file with the web explorer._



## Autor

* **Cognitus Foundation Students.** - [Cognitus Project](https://gitlab.com/andres613/cognitus-project.git)


[<img src="./.public/cognitus_first_class.jpg" alt="descriptive text"/>](https://web.facebook.com/cognitusorg "Cognitus Facebook.")