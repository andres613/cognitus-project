const express =  require('express');
const app = express();

app.use(express.json());

const cars = [
    {id: 1, brand: 'Mercedes'},
    {id: 2, brand: 'Crevrolet'}
];

app.get('/', (req, res) => {
    res.send('Hello Cognitus');
})

app.get('/cars', (req, res) =>{
    res.send(cars);
})

const port = process.env.PORT || 8080;
app.listen(port, ()=> console.log(`Escuchando por el puerto ${port}`));
